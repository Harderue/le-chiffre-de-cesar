import unittest

class TestCryptoFonction(unittest.TestCase):

    #!!!!!!!!!
    #Ne marche plus car les fonctions CodeUnePhrase et DecodeUnePhrase ne prennent plus de clee en parametres...
    #!!!!!!!!!

        #Code une phrase donner avec une clee donner
    def testCode(self):
        self.assertEqual(CodeUnePhrase("abc", 2), "cde")
         self.assertNotEqual(CodeUnePhrase("abc", 3), "cde")
        
        #Decode une phrase donner avec une clee donner
    def testDecode(self):
        self.assertEqual(DecodeUnePhrase("cde", 2), "abc")
         self.assertNotEqual(DecodeUnePhrase("cde", 3), "abc")
