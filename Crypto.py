try:
    #Python2
    from Tkinter import *   
except ImportError:
    #Python3
    from tkinter import *  

class Interface(Frame):

    def __init__(self, fenetre, **kwargs):
        Frame.__init__(self, fenetre, width=500, height=500, **kwargs)

        self.pack(fill=BOTH)
        self.TitreApp = Label(self, text="Le chiffre de cesar !", font=("Helvetica", 16), bg="gray")
        self.TitreApp.pack()

        #Choice circulaire ou non
        self.ResultatRadioBtn = StringVar()
        Radiobutton(self, text="Code circulaire", value = "circulaire", variable = self.ResultatRadioBtn).pack()
        Radiobutton(self, text="Code non circulaire", value = "nonCirculaire", variable = self.ResultatRadioBtn).pack()

        #CODE PART

            #Label
        self.messageCoderlabel = Label(self, text="Pour coder : ", font=("Helvetica", 12))
        self.messageCoderlabel.pack()

            #Message
        self.messageACoder = Entry(self)
        self.messageACoder.insert(0, "message a coder")
        self.messageACoder.pack()

            #Clee
        self.cleePourCoder = Entry(self)
        self.cleePourCoder.insert(0, "Key")
        self.cleePourCoder.pack()

            #Button
                #avec lambda
        self.buttoncoder = Button(self, text="Coder !", command= lambda : self.CodeUnePhrase(str(self.messageACoder.get())))
        self.buttoncoder.pack()

            #Resultat
                #utilisation de textvariable pour pouvoir changer le texte directement en changeant la variable ?
        self.resultatCode = Label(self, text = "", font=("Helvetica", 16))
        self.resultatCode.pack()

#---

        #DECODE part

            #Label
        self.messageDecoderlabel = Label(self, text="Pour decoder : ", font=("Helvetica", 12))
        self.messageDecoderlabel.pack()

            #Message
        self.messageADecoder = Entry(self)
        self.messageADecoder.insert(0, "message a decoder")
        self.messageADecoder.pack()

            #Clee
        self.cleePourDecoder = Entry(self)
        self.cleePourDecoder.insert(0, "Key")
        self.cleePourDecoder.pack()

            #Button
                #sans lambda
        self.buttonDecoder = Button(self, text="Decoder !", command=self.DecodeUnePhrase)
        self.buttonDecoder.pack()
                
            #Resultat
                #utilisation de textvariable pour pouvoir changer le texte directement en changeant la variable ?
        self.resultatDecode = Label(self, text = "", font=("Helvetica", 16))
        self.resultatDecode.pack()


#---
    #ici j'essaye de passer des arguments avec lambda, sauf la clee pour pouvoir faire le try-except dans la fonction.
    def CodeUnePhrase(self, unePhrase):

        #On verifie que la clee est valide (qu'on peut la mettre en int)
        try:
            int(self.cleePourCoder.get())
        except:
            print("invalid KEY !")
            return

        #on clear le resultat de decode si existe
        if self.resultatDecode['text'] != "":
            self.resultatDecode.config(text = "")

        phraseBrut = unePhrase
        taillePhrase = len(unePhrase)

        self.OnCodeCirculaire = False
        #si cocher -> true, sinon false, declarer avant
        if self.ResultatRadioBtn.get() == "circulaire":
            self.OnCodeCirculaire = True

        if self.OnCodeCirculaire == False:
            #print("Code NON circulaire.")
            clee = int(self.cleePourCoder.get())
        else:
            #print("Code circulaire.")
            clee = int(self.cleePourCoder.get()) % 26

        phraseCoder = ""
        #print("la clee : ", clee)

        #Traitement de la phrase caracteres par caracteres
        for i in range(0, taillePhrase):
            
            """if unePhrase[i] == ' ':
                temp = "_"
            elif unePhrase[i] == '.':
                temp = '*'"""
            #else:

            temp = ord(unePhrase[i])

            #si circulaire et majuscule -> premiere condition, si minuscule -> deuxieme, sinon non circulaire....
            if self.OnCodeCirculaire and (temp >= 65 and temp <= 90):
                temp += clee
                if temp > 90:
                    temp -= 26

            elif self.OnCodeCirculaire and (temp >= 97 and temp <= 122):
                temp += clee
                if temp > 122:
                    temp -= 26

            else:
                temp += clee
            
            #print("Temp : ", temp)
            temp = chr(temp)
            phraseCoder += temp

        #print("\nPhrase a coder : " + unePhrase)
        #print("\nPhrase coder : " + phraseCoder)

        self.resultatCode.config(text = str(phraseCoder))

        self.ResetCodeEntry()

        #print("Dans resultyatcodelabel : ", self.resultatCode.text)

    def ResetCodeEntry(self):
        #on clear les entry
        self.messageACoder.delete(0, 'end')
        self.messageACoder.insert(0, "message a coder")
        self.cleePourCoder.delete(0, 'end')
        self.cleePourCoder.insert(0, "Key")

#---
    #ici je test sans utiliser lambada, en utilisant les champs de etxte directement dans la fonction.
    def DecodeUnePhrase(self):
        
        #On verifie que la clee est valide (qu'on peut la mettre en int)
        try:
            int(self.cleePourDecoder.get())
        except:
            print("invalid KEY !")
            return

        #on clear le resultat de code
        if self.resultatCode['text'] != "":
            self.resultatCode.config(text = "")

        phraseChiffrer = str(self.messageADecoder.get()) 
        taillePhraseChiffrer = len(phraseChiffrer)

        self.OnCodeCirculaire = False
        #si cocher -> true, sinon false, declarer avant
        if self.ResultatRadioBtn.get() == "circulaire":
            self.OnCodeCirculaire = True

        if self.OnCodeCirculaire == False:
            #print("Code NON circulaire.")
            clee = int(self.cleePourDecoder.get())
        else:
            #print("Code circulaire.")
            clee = int(self.cleePourDecoder.get())%26

        phraseDecoder = ""
        #print("la clee : ", clee)

        #Traitement de la phrase caracteres par caracteres
        for i in range(0, taillePhraseChiffrer):
            temp = ord(phraseChiffrer[i])
            
            #si circulaire et majuscule -> premiere condition, si minuscule -> deuxieme, sinon non circulaire....
            if self.OnCodeCirculaire and (temp >= 65 and temp <= 90):
                temp -= clee
                if temp < 65:
                    temp += 26

            elif self.OnCodeCirculaire and (temp >= 97 and temp <= 122):
                temp -= clee
                if temp < 97:
                    temp += 26

            else:
                temp -= clee

            temp = chr(temp)
            phraseDecoder += temp

        #print("\nPhrase a decoder : " + phraseChiffrer)
        #print("\nPhrase decoder : " + phraseDecoder)

        
        self.resultatDecode.config(text = str(phraseDecoder))
        self.ResetDecodeEntry()
    
    def ResetDecodeEntry(self):
        #on clear les entry
        self.messageADecoder.delete(0, 'end')
        self.messageADecoder.insert(0, "message a coder")
        self.cleePourDecoder.delete(0, 'end')
        self.cleePourDecoder.insert(0, "Key")
        
#---
    """def mainFunction():

        clee = int(input("Clee : "))
        #Phrase = "ab cd."
        Phrase = input("Message a coder : ")

        while clee < 0 or clee > 256:
            clee = input("Clee non conforme (clee >= 0 && clee <= 256), reessayer : ")

        phraseApresCode = CodeUnePhrase(str(Phrase), int(clee))
        DecodeUnePhrase(str(phraseApresCode), clee)"""
#---

frame = Tk()
frame.title("Le chiffre de cesar")
frame.geometry("400x400")
frame.resizable(0, 0)

interface = Interface(frame)
interface.mainloop()
#interface.destroy()